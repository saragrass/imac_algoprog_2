#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

/***HuffmanNode est un noeud d’arbre possédant les attributs character, value et code qui sont respectivement
le caractère représentant le noeud, la fréquence d’apparition de ce caractères et le code
produit par le dictionnaire.
HuffmanHeap est un tas de HuffmanNode utilisant value (la fréquence d’un caractères) pour les
ordonner.***/

/***struct HuffmanNode{
    HuffmanNode* left;
    HuffmanNode* right;
    int value;
    char character;
    string code;
    void print()
    {
        if (this->left != nullptr)
            printf("left: %d with code: %s \n", this->left->value, this->left->code);
        if (this->right != nullptr)
            printf("right: %d with code: %s \n", this->right->value, this->right->code);
    printf("this: %d\n", this->value);
    }
};

class Heap : public Array{
    void print(); // declaration de la methode print de Heap
}

void Heap::print() // corps de la methode print de Heap
{
    for (i =0; i < this->size(); i++)
        printf("%d", this->get(i));
}

void Heap::clear() // corps de la methode clear de Heap
{
    for (i =0; i < this->size(); i++)
        this->set(i,0));
}***/

int Heap::leftChild(int nodeIndex)
{
    if (2*nodeIndex+1 <= int(size())){
        return this->get(2*nodeIndex+1);
    }
    return 0;
}

int Heap::rightChild(int nodeIndex)
{
    if ((2*nodeIndex+2 <= int(size()))){
            return this->get(2*nodeIndex+2);
    }
    return 0;
}

void Heap::insertHeapNode(int heapSize, int value)
{
    // use (*this)[i] or this->get(i) to get a value at index i
    int i = heapSize;
    (*this)[i] = value;
    while (i>0, (*this)[i] > (*this)[(i-1)/2]){
        swap((*this)[i], (*this)[(i-1)/2]);
        i = (i-1)/2;
    }
}

void Heap::heapify(int heapSize, int nodeIndex)
{
    // use (*this)[i] or this->get(i) to get a value at index i
    int i_max = nodeIndex;
    int largest = i_max;
    if ((*this)[i_max] < (*this)[2*i_max+1]){
        largest = 2*i_max+1;
    }
    else if ((*this)[i_max] > (*this)[2*i_max+2]){
        largest = 2*i_max+2;
    }
    if (largest != i_max){
        swap((*this)[i_max], (*this)[largest]);
        heapify(heapSize, largest);
    }
}

void Heap::buildHeap(Array& numbers)
{
    int heapSize = 0;
    insertHeapNode(heapSize, numbers[0]);
    for (int i = 1; i < int(numbers.size()); i++){
        insertHeapNode(heapSize, numbers[i]);
        heapify(heapSize, i);
        heapSize ++;
    }
}

void Heap::heapSort()
{
    int n = int((*this).size());
    for (int i = n-1; i > 0; i++){
        swap((*this)[0],(*this)[i]);
        heapify(i, 0);
    }
}


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
    w->show();

    return a.exec();
}
