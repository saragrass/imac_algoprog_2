#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w = nullptr;

void selectionSort(Array& toSort){
    // selectionSort
    int Indmin = 0;
    for (int j=0; j<int(toSort.size()-1); j++){
        for (int i=j; i<int(toSort.size()); i++){
            if (toSort[Indmin] >= toSort[i]){
                Indmin = i;
            }
        }
        toSort.swap(j,Indmin);
        Indmin = j+1;
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}
