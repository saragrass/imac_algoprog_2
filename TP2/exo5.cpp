#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& origin,int deb1,int fin1,int fin2);

void splitAndMerge(Array& origin, int indDebut, int indFin)
        {
        if (int(origin.size()) < 2)
            {
            int indMilieu=(indFin+indDebut)/2;
            splitAndMerge(origin,indDebut,indMilieu);
            splitAndMerge(origin,indMilieu+1,indFin);
            merge(origin,indDebut,indMilieu,indFin);
            }
        }

void merge(Array& origin,int deb1,int fin1,int fin2)
{
    int deb2=fin1+1;
    int compt1=deb1;
    int compt2=deb2;

Array& first = w->newArray(origin.size()/2);

    for (int i=deb1; i<=fin1; i++)
    {
        first[i-deb1]=origin[i];
    }

    for (int i=deb1; i<=fin2; i++)
    {
        if (compt1==deb2)
        {
            return;
        }
        else if (compt2==(fin2+1))
        {
            origin[i]=first[compt1-deb1];
            compt1++;
        }
        else if (first[compt1-deb1]<origin[compt2])
        {
            origin[i]=first[compt1-deb1];
            compt1++;
        }
        else
        {
            origin[i]=origin[compt2];
            compt2++;
        }
    }
}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort, 0, toSort.size()-1);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
