#include <QApplication>
#include <time.h>

#include "tp2.h"


MainWindow* w=nullptr;


int partition (Array& toSort, int inf, int sup)
{
    int x = toSort[sup];
    int i = inf - 1;
    for (int j = inf; j <= sup-1; j++)
    {
        if (toSort[j] <= x)
        {
            i++;
            toSort.swap (i, j);
        }
    }
    toSort.swap (i + 1, sup);
    return (i + 1);
}

void recursivQuickSort(Array& toSort, int inf, int sup)
{
    if (inf < sup)
    {
        int pivot = partition(toSort, inf, sup);
        recursivQuickSort(toSort, inf, pivot - 1);
        recursivQuickSort(toSort, pivot + 1, sup);
    }
}

void quickSort(Array& toSort){
    recursivQuickSort(toSort, 0, int(toSort.size())-1);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=20;
    MainWindow::instruction_duration = 50;
    w = new TestMainWindow(quickSort);
	w->show();

	return a.exec();
}
