#include <iostream>

using namespace std;

long power(long value, long n){
    if(n==0){
        return 1;
    }
    return(power(value, n-1)*value);
}

int main()
{
    long valeur, puissance;
    cout << "entrez un nombre et la puissance a laquelle vous voulez l'elever" << endl;
    cin >> valeur >> puissance;
    cout << "resultat = " << power(valeur, puissance) << endl;
}

