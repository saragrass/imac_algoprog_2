#include <iostream>

using namespace std;
int MAX_SIZE = 10;

int allEvens(int evens[], int array[], int evenSize, int arraySize){
    if (arraySize<0){
        return evenSize;
    }
    if (array[arraySize]%2==0){
        evens[evenSize] = array[arraySize];
        evenSize ++;
    }
    allEvens(evens, array, evenSize, arraySize-1);
}

int main(){
    int nombre;
    int tableau[MAX_SIZE];
    int tabPair[MAX_SIZE];
    char res = 'y';
    int ind = 0;
    while (res == 'y'){
        cout << "entrez un nombre a ajouter au tableau ? (y/n)" << endl;
        cin >> res;
        if (res == 'y'){
            cout << "nombre a ajouter ?" << endl;
            cin >> tableau[ind];
            ind ++;
        }
    }
    ind--;
    int tailletabPair = allEvens(tabPair, tableau, 0, ind);
    cout << "les elements du nouveau tableau avec que les nombres pairs de celui entre sont : " << endl;
    for (int j=0; j<tailletabPair; j++){
        cout << tabPair[j] << endl;
    }
}

