#include <iostream>
#include <stack>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    // your code
    int size;
};

struct DynaTableau{
    int* donnees;
    // your code
    int sizeTab;
    int capaciteTab;
};


void initialise(Liste* liste)
{
    int val;
    cout << "avec quelle valeur initialiser la liste ?" << endl;
    cin >> val;
    (*liste).premier->donnee = val;
    (*liste).premier->suivant = nullptr;
    (*liste).size = 1;
}

bool est_vide(const Liste* liste)
{
    if ((*liste).size == 0){
        return true;
    }else{
        return false;
    }
}

void ajoute(Liste* liste, int valeur)
{
    while ((*liste).premier->suivant != nullptr){
        (*liste).premier = (*liste).premier->suivant;
    }
    (*liste).premier->suivant->donnee = valeur;
    (*liste).premier->suivant->suivant = nullptr;
    (*liste).size ++;

}

void affiche(const Liste* liste)
{
    Liste Ltemp = *liste;
    while (Ltemp.premier->suivant != nullptr){
        cout << Ltemp.premier->donnee << endl;
        Ltemp.premier = Ltemp.premier->suivant;
    }
}

int recupere(const Liste* liste, int n)
{
    int i=0;
    Liste Ltemp = *liste;
    if ((*liste).size < n){
        return 0;
    }
    else{
        while (i<n){
            while (Ltemp.premier->suivant != nullptr){
                Ltemp.premier = Ltemp.premier->suivant;
            }
            i++;
        }
    }
    Ltemp.size ++;
    return Ltemp.premier->donnee;
}

int cherche(const Liste* liste, int valeur)
{
    int i=1;
    Liste Ltemp = *liste;
    while (i<Ltemp.size && Ltemp.premier->donnee != valeur){
        while (Ltemp.premier->suivant != nullptr){
            Ltemp.premier = Ltemp.premier->suivant;
        }
        i++;
    }
    if (i>Ltemp.size){
        return -1;
    }
    if (Ltemp.premier == nullptr){
        return -1;
    }
    return i;
}

void stocke(Liste* liste, int n, int valeur)
{
    int i=0;
    if ((*liste).size < n){
        cout << "erreur, pas assez d'elements" << endl;
    }
    else{
        while (i<n){
            while ((*liste).premier->suivant != nullptr){
                (*liste).premier = (*liste).premier->suivant;
            }
            i++;
        }
    }
    (*liste).premier->donnee = valeur;
}

void ajoute(DynaTableau* tableau, int valeur)
{
    if ((*tableau).sizeTab == (*tableau).capaciteTab){
        (*tableau).capaciteTab ++;
        DynaTableau *tableauTemp;
        tableauTemp = new DynaTableau[(*tableau).capaciteTab];
        (*tableauTemp).capaciteTab = (*tableau).capaciteTab;
        (*tableauTemp).sizeTab = (*tableau).capaciteTab;
        (*tableauTemp).donnees = (*tableau).donnees;
        tableau = tableauTemp;
    }
    (*tableau).donnees[(*tableau).sizeTab] = valeur;
    (*tableau).sizeTab ++;

}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau = new DynaTableau[capacite];
    (*tableau).capaciteTab = capacite;
    (*tableau).sizeTab = 0;
}

bool est_vide(const DynaTableau* liste)
{
    if ((*liste).sizeTab != 0){
        return false;
    } else{
        return true;
    }
}

void affiche(const DynaTableau* tableau)
{
    for (int i = 0; i<(*tableau).sizeTab; i++){
        cout << (*tableau).donnees[i] << endl;
    }
}

int recupere(const DynaTableau* tableau, int n)
{
    return (*tableau).donnees[n];
}

int cherche(const DynaTableau* tableau, int valeur)
{
    for (int i = 0; i<(*tableau).sizeTab; i++){
        if ((*tableau).donnees[i] == valeur){
            return i;
        }
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    (*tableau).donnees[n] = valeur;
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    Noeud *nouveau;
    nouveau->donnee = valeur;
    nouveau->suivant = nullptr;

    if (liste->premier != nullptr){
        Noeud *eltActuel = liste->premier;
        while (eltActuel->suivant != nullptr)
        {
            eltActuel = eltActuel->suivant;
        }
        eltActuel->suivant = nouveau;
    }
    else{
        liste->premier = nouveau;
    }
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    int nombreDefile = 0;
    if (liste->premier != NULL)
    {
        Noeud *noeud = liste->premier;

        nombreDefile = noeud->donnee;
        liste->premier = noeud->suivant;
        free(noeud);
    }

    return nombreDefile;
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{
    Noeud *noeud;
    noeud->donnee = valeur;
    noeud->suivant = liste->premier;
    liste->premier = noeud;
}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste)
{
    //(*liste).premier->donnee.pop();
    while (liste->premier->suivant != nullptr)
    {
        liste->premier = liste->premier->suivant;
    }
    int eltARetirer = liste->premier->donnee;
    liste->premier = nullptr;
    return eltARetirer;
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
