#include <iostream>

using namespace std;
int MAX_SIZE = 10;

int search(int value, int array[], int size){
    if (array[size] == value){
        return size;
    }
    search(value, array, size-1);
}

int main(){
    int valeurRecherchee;
    int nombre;
    int tableau[MAX_SIZE];
    char res = 'y';
    int i = 0;
    while (res == 'y'){
        cout << "entrez un nombre a ajouter au tableau ? (y/n)" << endl;
        cin >> res;
        if (res == 'y'){
            cout << "nombre a ajouter ?" << endl;
            cin >> tableau[i];
            i ++;
        }
    }
    cout << "nombre recherche ?" << endl;
    cin >> valeurRecherchee;
    cout << "indice du tableau ou se trouve le nombre recherche : " << search(valeurRecherchee, tableau, i) << endl;
}

