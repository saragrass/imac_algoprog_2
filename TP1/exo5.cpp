#include <iostream>

using namespace std;

typedef struct vec2{
    int x;
    int y;
} vec2;

int xtemp;

bool mandelbrot(vec2 z, vec2 point, int n){
    if (z.x*z.x + z.y*z.y > 2*2){
        return false;
    }
    else if (n==0){
        return true;
    }
    if (z.x*z.x + z.y*z.y <= 2*2 && 0 < n){
        xtemp = z.x*z.x - z.y*z.y + point.x;
        z.y = 2*z.x*z.y + point.y;
        z.x = xtemp;
    }
    mandelbrot(z, point, n-1);
}

int main(){
    vec2 z;
    vec2 point;
    int MAX_ITERATION;
    cout << "entrez n" << endl;
    cin >> MAX_ITERATION;
    cout << "entrez les deux coordonnees de z" << endl;
    cin >> z.x >> z.y;
    cout << "entrez les deux coordonnees du point" << endl;
    cin >> point.x >> point.y;
    cout << "le point appartient-il a l'ensemble de mandelbrot ? (0/1) " << mandelbrot(z, point, MAX_ITERATION) << endl;
}
