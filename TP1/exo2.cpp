#include <iostream>

using namespace std;

float fibonacci(const int n){
    if (n==0)
        return 0;
    if (n==1)
        return 1;
    return fibonacci(n-2)+fibonacci(n-1);
}

int main()
{
    int nombre;
    cout << "entrez un nombre" << endl;
    cin >> nombre;
    cout << "resultat = " << fibonacci(nombre) << endl;
}

